# CPP

## Principe S : 

- La classe Jeu.cpp de Raphaël a deux responsabilités : gérer le jeu mais aussi les joueurs. Cette classe ne respecte donc pas le principe S.
```cpp
class Jeu
{
public:
	Jeu(); // Constructeur
	~Jeu(); // Destructeur
	GrilleMorpion morpion; // Appel de la classe GrilleMorpion pour utiliser les fonctions 
	Jeu(int nombre, string nom); // numéro du joueur et le pseudo 
	void renommer(string nom); // renommer un joueur quand rematch 
	string getNom() const;
	int getNombre() const;
	void SaisieJoueur(); // saisie des joueurs 

private:
	string c_nom;
	int c_nombre;
	int joue = 1;
	string nom;
};
```

- Alors que dans le projet de Fanny il y a une classe Jeu.cpp qui se charge de gérer la partie de jeu et une classe Joueur.cpp qui s'occupe des joueurs. De cette façon le principe S est mieux respecté.

```cpp
class Joueur
{
public:
    Joueur(const int id);

    int getIdentifiant() const
    {
        return identifiant;
    }

    const std::string getNom() const
    {
        return nom;
    }

    void setNom(const std::string &newNom)
    {
        nom = newNom;
    }


private:
    const int identifiant;
    std::string nom;
};
```

```cpp
class Jeu
{
public:
    Jeu(Grille* _grille);

private:
    Joueur j1=Joueur(1);
    Joueur j2=Joueur(2);
    Grille* grille;
    void deroulementPartie();
    void debutPartie();
    bool choixCase(const int joueur);
    bool stringIsInt(const std::string str) const;
    bool verificationSaisie(const int i) const;
    bool finPartie();
    void felicitation(const int gagnant) const;
    void matchNul();
};
```

## Principe O :

- Dans le code de Raphaël le principe O n'est pas respecté car pour ajouter le puissance4 il faudrait modifier la méthode SaisieJoueur de sa classe Jeu.cpp 

```cpp
void Jeu::SaisieJoueur() // boucle pour savoir quand le joueur gagne ou continue jusqu'à ce que l'une des conditions soient remplis 
{
    int x = 0; // Lignes
    int y = 0; // Colonnes
    cout << "Veuillez entrer un nom pour le joueur 1" << endl;
    cin >> nom;
    Jeu j1(1, nom);

    cout << "Veuillez entrer un nom pour le joueur 2" << endl;
    cin >> nom;
    Jeu j2(2, nom);
    while (morpion.VictoireJoueur() == false || morpion.GetCoup() < 9 )
    {
        cout << "A votre tour, joueur " << joue << ":" << endl;
        cout << "Valeur de x :" << endl;
        cin >> x;
        cout << "Valeur de y :" << endl;
        cin >> y;
        morpion.DepotJeton(joue, x, y);
        morpion.AffichageGrille();
        if (morpion.VictoireJoueur() == true && joue == 1)
            cout << endl << j1.getNom() << " a gagne" << endl;
        else if (morpion.VictoireJoueur() == 1 && joue == 2)
            cout << endl << j2.getNom() << " a gagne" << endl;
        else if (morpion.GetCoup() == 9)
        {
            cout << "Match nul" << endl; // Match nul si égalité
        }
        else if (joue == 1)
            joue = 2;
        else
            joue = 1;
    }
    
}
```

- Dans la classe Jeu de Fanny, il y a la méthode choixCase. Dans cette méthode le principe O est mal appliqué en effet il y a une condition sur le type de grille mais si on rajoute par exemple une grilleDame il faudra modifier la méthode.

```cpp
if (dynamic_cast<GrilleMorpion*>(this->grille) != 0){
        std::string ligne;
        std::cout<<"Joueur "<<nom<<", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::cin>>ligne>>colonne;
        if(!stringIsInt(ligne) || !stringIsInt(colonne)){
            return false;
        }
        int l=std::stoi(ligne);
        int c=std::stoi(colonne);

        if(verificationSaisie(l)&&verificationSaisie(c)){
            return grille->deposerJeton(joueur,c,l);
        }
        return false;
    }
    else if (dynamic_cast<GrillePuissance4*>(this->grille) != 0){

        std::cout<<"Joueur "<<joueur<<", entre le numero de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::cin>>colonne;
        if(!stringIsInt(colonne)){
            return false;
        }
        int c=std::stoi(colonne);

        if(verificationSaisie(c)){
            return grille->deposerJeton(joueur,c);
        }
        return false;
    }
```

## Principe L : 

- Dans le code de Raphaël il n'y a pas d'héritage, donc ce principe n'est pas appliqué.
- Dans le code de Fanny, les classes grilleMorpion et grillePuissance4 hérite de la classe parent grille, donc le principe L est respécté.
```cpp
class GrilleMorpion:public Grille
{
public:
    GrilleMorpion();
    bool deposerJeton(const int joueur,const int j,const int i=-1);

};
```

```cpp
class GrillePuissance4:public Grille
{
public:
    GrillePuissance4();
    bool deposerJeton(const int joueur,const int j,const int i=-1);
};
```

## Principe I : 
- Dans le projet de Raphaël, le principe I dépend du principe S. On a vu dans l'exemple du principe S qu'il n'était pas respecté, donc ce principe n'est pas respecté non plus.
- Dans le projet de Fanny il y a une méthode qui est redéfinie dans grilleMorpion.cpp et grillePuissance4.cpp cependant une argument de cette méthode est unitilisé dans grillePuissance4.cpp ce qui prouve une mauvaise gestion des interfaces et le non-respect de ce principe.

```cpp
bool GrillePuissance4::deposerJeton(const int joueur, const int j, const int i) //le paramètre i n'est jamais utilisé 
{
    for(int i=NB_LIGNES-1;i>=0;i--){
        if(this->isVide(i,j)){
            this->grille[i][j]=joueur;
            return true;
        }
    }

    return false;
}
```

## Principe D : 

- Dans le code de Raphaël il n'y a que des classes de bas niveau
- Dans le code de Fanny il y a l'utilisation d'une classe de haut niveau : grille.cpp, et des classes de bas niveau : grilleMorpion.cpp et grillePuissance4.cpp. Cependant il n'y a pas d'interfaces qui fait le lien entre les deux.

## Tell don't ask 

- Le code de Raphaël et de Fanny respect le principe Tell don't ask car aucun objet n'intéragit avec un autre pour obtenir une
réponse sur son propre état.

## Loi de demeter 
- Le code de Raphaël et de Fanny respecte la loi de demeter car aucun objet ne fait de pont d'un objet à un autre.
