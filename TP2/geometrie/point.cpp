#include "point.h"

Point::Point():x(0),y(0){
}

Point::Point(float _x,float _y):x(_x),y(_y){

}

float Point::calculDistance(Point point1) const{
    return sqrt(powf(point1.x-x,2)+powf(point1.y-y,2));
}

void Point::Afficher() const{
    std::cout<<"x="<<this->x<<", y="<<this->y<<std::endl;
    std::cout<<"fonction calculDistance(Point p) : calcul la distance d'un point passe en parametre par rapport a celui-ci"<<std::endl;
}
