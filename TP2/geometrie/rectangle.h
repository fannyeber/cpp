#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point.h"

class Rectangle
{
public:
    //constructeur et destructeur
    Rectangle();
    Rectangle(float _largeur, float _longueur, Point _supGauche);

    //getters et setters
    int getLargeur() const{
        return this->largeur;
    }
    int getLongueur() const{
        return this->longueur;
    }
    Point getPoint() const{
        return this->superieurGauche;
    }
    void setLargeur(float nouvelleLargeur){
        this->largeur=nouvelleLargeur;
    }
    void setLongueur(float nouvelleLongueur){
        this->longueur=nouvelleLongueur;
    }
    void setSuperieurGauche(Point nouveauPoint){
        this->superieurGauche=nouveauPoint;
    }

    //méthodes publics
    float calculPerimetre() const;
    float calculSurface() const;

    void Afficher() const;

private:
    float longueur;
    float largeur;
    Point superieurGauche;
};

#endif // RECTANGLE_H
