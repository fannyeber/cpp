#include "point.h"
#include "cercle.h"
#include "rectangle.h"
#include "triangle.h"


int main()
{
    //Verification de Point
    Point p={0,1};
    Point p2={1,0};
    Point p3={1,1};
    p.Afficher();

    //Verificaton de rectangle
    Rectangle r= Rectangle(1.25,2,p3);
    r.Afficher();
    //Verification de cercle
    Cercle c=Cercle(0.5,p3);
    c.Afficher();
    //Verification de triangle
    Triangle t=Triangle(p,p2,p3);
    t.Afficher();
}
