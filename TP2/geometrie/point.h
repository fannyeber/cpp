#ifndef POINT_H
#define POINT_H

#include <math.h>
#include <iostream>

struct Point{
    Point();
    Point(float x, float y);

    float x;
    float y;

    float calculDistance(Point point1) const;
    void Afficher() const;
};


#endif // POINT_H
