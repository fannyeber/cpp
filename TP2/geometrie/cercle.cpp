#include "cercle.h"

Cercle::Cercle():diametre(1), centre(Point()){

}

Cercle::Cercle(float _diametre, Point _centre):diametre(_diametre),centre(_centre)
{

}

float Cercle::calculPerimetre() const{
    return M_PI*this->diametre;
}

float Cercle::calculSurface() const{
    return M_PI*powf((this->diametre/2),2);
}

float Cercle::calculDistanceDuCentre(Point point1) const{
    return this->getCentre().calculDistance(point1);
}

bool Cercle::estSurCercle(Point point) const{
    return calculDistanceDuCentre(point)==this->diametre/2;
}

bool Cercle::estDansCercle(Point point) const{
    return calculDistanceDuCentre(point)<this->diametre/2;
}

void Cercle::Afficher() const{
    std::cout<<"Centre du cercle : ";
    this->getCentre().Afficher();

    std::cout<<"Diametre : "<<this->getDiametre()<<std::endl;

    std::cout<<"Perimetre : "<<this->calculPerimetre()<<std::endl;

    std::cout<<"Surface : "<<this->calculSurface()<<std::endl;
    std::cout<<"estSurCercle(Point p) Verifie si le point passe en parametre est sur le cercle \n"
               "estDansCercle(Point p) Verifie si le point passe en parametre est dans le cercle \n"
               "calculDistanceDuCentre(Point p) Calcul le distance du point passe en parametre du cercle"<<std::endl;
}

