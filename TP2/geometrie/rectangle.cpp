#include "rectangle.h"

Rectangle::Rectangle(): longueur(1),largeur(1), superieurGauche(Point()){

}

Rectangle::Rectangle(float _largeur, float _longueur, Point _supGauche):longueur(_longueur),largeur(_largeur), superieurGauche(_supGauche)
{

}

float Rectangle::calculPerimetre()const{
    return this->longueur*2+this->largeur*2;
}

float Rectangle::calculSurface()const{
    return this->longueur*this->largeur;
}

void Rectangle::Afficher()const{
    std::cout<<"Longueur : "<<this->getLongueur()<<std::endl;
    std::cout<<"Largeur : "<<this->getLargeur()<<std::endl;
    std::cout<<"Point superieur gauche : ";
    this->getPoint().Afficher();
    std::cout<<"Perimetre : "<<this->calculPerimetre()<<std::endl;
    std::cout<<"Surface : "<<this->calculSurface()<<std::endl;
}
