#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "point.h"
#include <array>

class Triangle
{

public:
    //constructeur et destructeur
    Triangle(Point a,Point b, Point c);

    //getters et setters
    Point getPointA()const{
        return this->pointA;
    }
    Point getPointB()const{
        return this->pointB;
    }
    Point getPointC()const{
        return this->pointC;
    }
    void setPointA(Point nouveauPoint){
        this->pointA=nouveauPoint;
    }
    void setPointB(Point nouveauPoint){
        this->pointB=nouveauPoint;
    }
    void setPointC(Point nouveauPoint){
        this->pointC=nouveauPoint;
    }

    static const int NB_COTE=3;

    //methodes
    std::array<float,NB_COTE> getLongueurs()const;
    float getBase()const;
    float getHauteur()const;
    bool estIsocele()const;
    bool estRectangle()const;
    bool estEquilateral()const;
    float getSurface()const;
    float getPerimetre()const;

    void Afficher() const;

private:
    Point pointA,pointB,pointC;


};

#endif // TRIANGLE_H
