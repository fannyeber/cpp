#ifndef CERCLE_H
#define CERCLE_H

#include "point.h"
#include <math.h>

class Cercle
{
public:
    //constructeur et destructeur
    Cercle();
    Cercle(float _diametre, Point _centre);

    //getters et setters
    float getDiametre() const{
        return this->diametre;
    }
    Point getCentre() const{
        return this->centre;
    }
    void setDiametre(float nouveauDiametre){
        this->diametre=nouveauDiametre;
    }
    void setCentre(Point nouveauPoint){
        this->centre=nouveauPoint;
    }

    //méthodes publics
    float calculPerimetre() const;
    float calculSurface() const;
    float calculDistanceDuCentre(Point point) const;
    bool estSurCercle(Point point) const;
    bool estDansCercle(Point point) const;
    void Afficher() const;

private:
    float diametre;
    Point centre;
};

#endif // CERCLE_H
