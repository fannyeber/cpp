#include "triangle.h"

Triangle::Triangle(Point a,Point b, Point c): pointA(a), pointB(b), pointC(c)
{

}

std::array<float,3> Triangle::getLongueurs()const{
    std::array<float,NB_COTE> longueurs={
        this->pointA.calculDistance(pointB),
        this->pointB.calculDistance(pointC),
        this->pointC.calculDistance(pointA)};
    return longueurs;
}

float Triangle::getBase()const{
    std::array<float,NB_COTE> longueurs=this->getLongueurs();
    float base= longueurs[0]>longueurs[1] ? longueurs[0] : longueurs[1];
    return longueurs[2]>base ? longueurs[2] : base;
}

float Triangle::getHauteur()const{
    return ((2.0 * getSurface()) /  getBase());
}


bool Triangle::estIsocele()const{
    std::array<float,NB_COTE> longueurs=this->getLongueurs();
    return (longueurs[0]==longueurs[1] || longueurs[0]==longueurs[2] || longueurs[1]==longueurs[2]);
}

bool Triangle::estRectangle()const{
    std::array<float,NB_COTE> longueurs = getLongueurs();
    float ab = powf(longueurs[0], 2);
    float bc = powf(longueurs[1], 2);;
    float ca = pow(longueurs[2], 2);;

    return ((ab+bc) == ca || (ab+ca) == bc || (bc+ca == ab));
}

bool Triangle::estEquilateral()const{
    std::array<float,NB_COTE> longueurs=this->getLongueurs();
    return (longueurs[0]==longueurs[1] && longueurs[1]==longueurs[2]);
}

float Triangle::getSurface()const{
    float surface= ((pointB.x - pointA.x)*(pointC.y - pointA.y) - (pointC.x - pointA.x)*(pointB.y - pointA.y)) / 2;
    return fabs(surface);
}

float Triangle::getPerimetre()const{
    std::array<float,NB_COTE> longueurs=getLongueurs();
    return longueurs[0]+longueurs[1]+longueurs[2];
}

void Triangle::Afficher()const{
    std::cout<<"Point A : ";
    this->getPointA().Afficher();
    std::cout<<"Point B : ";
    this->getPointB().Afficher();
    std::cout<<"Point C : ";
    this->getPointC().Afficher();

    std::array<float,NB_COTE> longueurs=getLongueurs();
    std::cout<<"Distances AB="<<longueurs[0]<<", BC="<<longueurs[1]<<", CA="<<longueurs[2]<<std::endl;

    std::cout<<"Perimetre : "<<this->getPerimetre()<<" Surface : "<< this->getSurface()<<std::endl;
    std::cout<<"Hauteur : "<<this->getHauteur()<<" Base : "<<this->getBase()<<std::endl;

    if(this->estEquilateral()){
        std::cout<<"Ce triangle est equilateral"<<std::endl;
    }
    else{
        std::cout<<"ce triangle n'est PAS equilateral"<<std::endl;
    }

    if(this->estIsocele()){
        std::cout<<"Ce triangle est isocele"<<std::endl;
    }
    else{
        std::cout<<"ce triangle n'est PAS isocele"<<std::endl;
    }

    if(this->estRectangle()){
        std::cout<<"Ce triangle est rectangle"<<std::endl;
    }
    else{
        std::cout<<"ce triangle n'est PAS rectangle"<<std::endl;
    }
}
