#include "jeupuissance4.h"

JeuPuissance4::JeuPuissance4(Joueur& joueur1,Joueur& joueur2): IJeuPuissance4(joueur1,joueur2)
{
    deroulementPartie();
}

/**
 * @brief Jeu::deroulementPartie : ensemble des étapes dans un jeu
 */
void JeuPuissance4::deroulementPartie()
{
    //debutPartie();
    grille.afficheGrille();

    //Commencer la partie avec un tour par tour et une verification de la victoire à chaque tour
    bool fin=false;
    bool deposeJeton=false;
    while(!fin){ //Tant que aucun joueur a gagné ou qu'il n'y a pas de match nul on continue à jouer
        for(int i=1;i<3 && !fin ;i++){ //car il y a deux joueurs
            while(!deposeJeton){ //Tant que le joueur n'a pas réussi a poser un jeton il pourra rééssayer a poser
                deposeJeton=choixCase(i);
                if(!deposeJeton){
                    std::cout<<"ERREUR : Il faut rentrer un chiffre entre 0 et "<<this->grille.getNB_COLONNES()<<" sur une case vide"<<std::endl;
                }
            }

            grille.afficheGrille();
            fin=this->finPartie(); //on verifie si quelqu'un a gagné ou si il y a match nul
            deposeJeton=false;
        }
    }

}

/**
 * @brief Jeu::choixCase : demande au joueur de placer son pion sur une case
 * en saisissant ligne et colonne ou seulement colonne en fonction de la grille
 * @param joueur : indice du joueur
 * @return vrai si le joueur a réussi a placer son jeton, faux sinon
 */
bool JeuPuissance4::choixCase(const int joueur)
{
    std::string nom;
    if(joueur==1){
        nom=j1.getNom();
    }
    else{
        nom=j2.getNom();
    }
    std::string colonne;
    std::string ligne;

    std::cout<<"Joueur "<<joueur<<", entre le numero de la colonne ou tu veux mettre ton jeton :"<<std::endl;
    std::cin>>colonne;
    if(!stringIsInt(colonne)){
        return false;
    }
    int c=std::stoi(colonne);

    if(verificationSaisieColonne(c)){
        return static_cast<GrillePuissance4&>(grille).deposerJeton(joueur,c);
    }
    return false;

}
