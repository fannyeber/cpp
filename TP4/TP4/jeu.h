#ifndef JEU_H
#define JEU_H

#include "joueur.h"
#include "grille.h"
#include <iostream>
#include <stdlib.h>
#include <string>

class Jeu
{
public:
    Jeu(Grille& _grille, Joueur& joueur1, Joueur& joueur2);
    virtual void deroulementPartie()=0;

protected:
    Joueur& j1;
    Joueur& j2;
    Grille& grille;
    bool stringIsInt(const std::string str) const;
    bool verificationSaisieColonne(const int j) const;
    bool verificationSaisieLigne(const int i) const;
    bool finPartie();

private:
    void felicitation(const int gagnant) const;
    void matchNul();
};

#endif // JEU_H
