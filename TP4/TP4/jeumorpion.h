#ifndef JEUMORPION_H
#define JEUMORPION_H

#include "ijeumorpion.h"

class JeuMorpion:public IJeuMorpion
{
public:
    JeuMorpion(Joueur& j1,Joueur& j2);
    void deroulementPartie();
    bool choixCase(const int joueur);
};

#endif // JEUMORPION_H
