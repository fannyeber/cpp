#include "grilleothello.h"

 GrilleOthello:: GrilleOthello() : IGrilleOthello()
{
     this->initOthello();
}

 /**
 * @brief GrilleOthello::initOthello dépose les jetons pour commencer la partie
 */
void GrilleOthello::initOthello(){
 grille[3][3]=1;
 grille[4][4]=1;
 grille[3][4]=2;
 grille[4][3]=2;
}

/**
 * @brief possibiliteDeJouer
 * @param joueur
 * @return vrai si le joueur peut jouer faux sinon
 */
bool GrilleOthello::possibiliteDeJouer(const int joueur){
    int ennemi;
    if(joueur==1){
        ennemi=2;
    }
    else{
        ennemi=1;
    }

    for(int i=0;i<NB_LIGNES;i++){
        for(int j=0;j<NB_COLONNES;j++){
            if(grille[i][j]==ennemi){
                //verification qu'il y a une place autour de l'ennemi et si  l'ennemi est encadrable
                if(this->isVide(i-1,j-1)){
                    if(encadrementDiagonal(joueur,i-1,j-1,false) ||
                            encadrementHorizontal(joueur,i-1,j-1,false) ||
                            encadrementVertical(joueur,i-1,j-1,false)){
                        return true;
                    }
                }
                if(this->isVide(i-1,j)){
                    if(encadrementDiagonal(joueur,i-1,j,false) ||
                            encadrementHorizontal(joueur,i-1,j,false) ||
                            encadrementVertical(joueur,i-1,j,false)){
                        return true;
                    }
                }
                if(this->isVide(i-1,j+1)){
                    if(encadrementDiagonal(joueur,i-1,j+1,false) ||
                            encadrementHorizontal(joueur,i-1,j+1,false) ||
                            encadrementVertical(joueur,i-1,j+1,false)){
                        return true;
                    }
                }
                if(this->isVide(i,j-1)){
                    if(encadrementDiagonal(joueur,i,j-1,false) ||
                            encadrementHorizontal(joueur,i,j-1,false) ||
                            encadrementVertical(joueur,i,j-1,false)){
                        return true;
                    }
                }
                if(this->isVide(i,j+1)){
                    if(encadrementDiagonal(joueur,i,j+1,false) ||
                            encadrementHorizontal(joueur,i,j+1,false) ||
                            encadrementVertical(joueur,i,j+1,false)){
                        return true;
                    }
                }
                if(this->isVide(i+1,j-1)){
                    if(encadrementDiagonal(joueur,i+1,j-1,false) ||
                            encadrementHorizontal(joueur,i+1,j-1,false) ||
                            encadrementVertical(joueur,i+1,j-1,false)){
                        return true;
                    }
                }
                if(this->isVide(i+1,j)){
                    if(encadrementDiagonal(joueur,i+1,j,false) ||
                            encadrementHorizontal(joueur,i+1,j,false) ||
                            encadrementVertical(joueur,i+1,j,false)){
                        return true;
                    }
                }
                if(this->isVide(i+1,j+1)){
                    if(encadrementDiagonal(joueur,i+1,j+1,false) ||
                            encadrementHorizontal(joueur,i+1,j+1,false) ||
                            encadrementVertical(joueur,i+1,j+1,false)){
                        return true;
                    }
                }


            }
        }
    }
    return false;
}

/**
 * @brief GrilleOthello::encadrementHorizontal
 * @param joueur celui qui pose le jeton
 * @param i indice de ligne dans le tableau
 * @param j indice de colonne dans le tableau
 * @param flip si vraie alors on retourne les jetons ennemi sinon non
 * @return vrai si le jeton en i j du joueur encadre un ennemi, faux sinon
 */
bool GrilleOthello::encadrementHorizontal(const int joueur,const int i, const int j, const bool flip){
    bool isFlip1=false;
    bool fin=false;

    if(this->isJoueur(joueur,i,j+1) || this->isVide(i,j+1)){
        fin=true;
    }

    for(int y=j+1;y<NB_COLONNES && !isFlip1 && !fin;y++){
        if(this->grille[i][y]==joueur){
            isFlip1=true;
            if(flip){
                for(int k=y;k>j;k--){
                    grille[i][k]=joueur;
                }
            }

        }
        if(this->isVide(i,y)){
            fin=true;
        }
    }
    fin=false;
    bool isFlip2=false;


    if(this->isJoueur(joueur,i,j-1) || this->isVide(i,j-1)){
        fin=true;
    }

    for(int y=j-1;y>=0 && !isFlip2 && !fin;y--){
        if(this->grille[i][y]==joueur){
            isFlip2=true;
            if(flip){
                for(int k=y;k<j;k++){
                    grille[i][k]=joueur;
                }
            }
        }
        if(this->isVide(i,y)){
            fin=true;
        }
    }

    return isFlip1 ||isFlip2;
}

/**
 * @brief GrilleOthello::encadrementVertical
 * @param joueur celui qui pose le jeton
 * @param i indice de ligne dans le tableau
 * @param j indice de colonne dans le tableau
 * @param flip si vraie alors on retourne les jetons ennemi sinon non
 * @return vrai si le jeton en i j du joueur encadre un ennemi, faux sinon
 */
bool GrilleOthello::encadrementVertical(const int joueur,const int i, const int j,const bool flip){

    bool fin=false;
    bool isFlip1=false;

    if(this->isJoueur(joueur,i+1,j) || this->isVide(i+1,j)){
        fin=true;
    }

    for(int x=i+1;x<NB_LIGNES && !isFlip1 && !fin;x++){
        if(this->grille[x][j]==joueur){
            isFlip1=true;
            if(flip){
                int k=x;
                for(int x=k;x>i;x--){
                    grille[x][j]=joueur;
                }
            }

        }
        if(this->isVide(x,j)){
            fin=true;
        }
    }
    fin=false;
    bool isFlip2=false;

    if(this->isJoueur(joueur,i-1,j) || this->isVide(i-1,j)){
        fin=true;
    }

    for(int x=i-1;x>=0 && !isFlip2 && !fin;x--){
        if(this->grille[x][j]==joueur){
            isFlip2=true;
            if(flip){
                int k=x;
                for(int x=k;x<i;x++){
                    grille[x][j]=joueur;
                }
            }
        }
        if(this->isVide(x,j)){
            fin=true;
        }
    }

    return isFlip1 ||isFlip2;
}

/**
 * @brief GrilleOthello::encadrementDiagonal
 * @param joueur celui qui pose le jeton
 * @param i indice de ligne dans le tableau
 * @param j indice de colonne dans le tableau
 * @param flip si vraie alors on retourne les jetons ennemi sinon non
 * @return vrai si le jeton en i j du joueur encadre un ennemi, faux sinon
 */
bool GrilleOthello::encadrementDiagonal(const int joueur,const int i, const int j,const bool flip){

    bool isFlip1=false;
    bool fin=false;

    if(this->isJoueur(joueur,i+1,j+1) || this->isVide(i+1,j+1)){
        fin=true;
    }

    int x=i+1;
    for(int y=j+1;y<NB_COLONNES && x<NB_LIGNES && !isFlip1 && !fin;y++){
        if(this->grille[x][y]==joueur){
            isFlip1=true;
            if(flip){
                int k=x;
                for(int k2=y;k>i && k2>j;k2--){
                    grille[k][k2]=joueur;
                    k--;
                }

            }

        }
        if(this->isVide(x,y)){
            fin=true;
        }
        x++;
    }

    fin=false;
    bool isFlip2=false;

    if(this->isJoueur(joueur,i-1,j-1) || this->isVide(i-1,j-1)){
        fin=true;
    }

    x=i-1;
    for(int y=j-1;y>=0 && x>=0 && !isFlip2 && !fin;y--){
        if(this->grille[x][y]==joueur){
            isFlip2=true;
            if(flip){
                int k=x;
                for(int k2=y;k2<j && k<i;k2++){
                    grille[k][k2]=joueur;
                    k++;
                }

            }
        }
        if(this->isVide(x,y)){
            fin=true;
        }
        x--;
    }

    fin=false;
    bool isFlip3=false;

    if(this->isJoueur(joueur,i-1,j+1) || this->isVide(i-1,j+1)){
        fin=true;
    }

    x=i-1;
    for(int y=j+1;y<NB_COLONNES && x>=0 && !isFlip3 && !fin;y++){
        if(this->grille[x][y]==joueur){
            isFlip3=true;
            if(flip){
                int k=x;
                for(int k2=y;k2>j && k<i;k2--){
                    grille[k][k2]=joueur;
                    k++;
                }
            }
        }
        if(this->isVide(x,y)){
            fin=true;
        }
        x--;
    }

    fin=false;
    bool isFlip4=false;

    if(this->isJoueur(joueur,i+1,j-1) || this->isVide(i+1,j-1)){
        fin=true;
    }

    x=i+1;
    for(int y=j-1;y>=0 && x<NB_LIGNES && !isFlip4 && !fin;y--){
        if(this->grille[x][y]==joueur){
            isFlip4=true;
            if(flip){
                int k=x;
                for(int k2=y;k2<j && k>i;k2++){
                    grille[k][k2]=joueur;
                    k--;
                }
            }
        }
        if(this->isVide(x,y)){
            fin=true;
        }
        x++;
    }
    return isFlip1 || isFlip2 || isFlip3 || isFlip4;
}

/**
 * @brief GrilleOthello::deposerJeton
 * @param joueur celui qui pose le jeton
 * @param j indice de la colonne du tableau
 * @param i indice de la ligne du tableau
 * @return vrai si le joueur a reussi a poser le jeton, faux sinon
 */
bool GrilleOthello::deposerJeton(const int joueur,const int j,const int i)
{
    int ennemi;
    if(joueur==1){
        ennemi=2;
    }
    else{
        ennemi=1;
    }

    if(!this->isVide(i,j)){
        return false;
    }

    bool vertical=false;
    bool horizontal=false;
    bool diagonale=false;
    if(this->isJoueur(ennemi,i-1,j-1) || this->isJoueur(ennemi,i-1,j+1) || this->isJoueur(ennemi,i+1,j-1) || this->isJoueur(ennemi,i+1,j+1)){
        diagonale=encadrementDiagonal(joueur,i,j,true);
    }

    if(this->isJoueur(ennemi,i,j-1) || this->isJoueur(ennemi,i,j+1)){
        horizontal=encadrementHorizontal(joueur,i,j,true);
    }

    if(this->isJoueur(ennemi,i-1,j) || this->isJoueur(ennemi,i+1,j)){
        vertical=encadrementVertical(joueur,i,j,true);
    }

    if(horizontal||vertical||diagonale){
        this->grille[i][j]=joueur;
        return true;
    }
    return false;
}

/**
 * @brief GrilleOthello::victoireJoueur
 * @param joueur indice du joueur
 * @return vrai si le joueur a gagné, faux sinon
 */
bool GrilleOthello::victoireJoueur(const int joueur) const{
    int jetonJoueur=0;
    int jetonEnnemi=0;
    for(int i=0;i<NB_LIGNES ;i++){
        for(int j=0;j<NB_COLONNES;j++){
            if(!this->isJoueur(joueur,i,j)){
                jetonEnnemi++;
            }
            else{
                jetonJoueur++;
            }
        }
    }
    //si la grille est rempli et que le joueur a le plus de jeton
    if(this->isRempli() && jetonJoueur>32){
        return true;
    }
    if(jetonEnnemi==0){
        return true;
    }
    return false;
}
