#include "grille.h"

class IGrilleOthello : public Grille
{
public:

    IGrilleOthello();
    virtual bool deposerJeton(const int joueur,const int j,const int i) = 0;
    virtual bool victoireJoueur(const int joueur) const=0;
};

