#include "grille.h"

class Igrillemorpion : public Grille
{
public:

	Igrillemorpion();
	virtual bool deposerJeton(const int joueur, const int j, const int i = -1) = 0;
};

