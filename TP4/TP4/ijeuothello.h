#ifndef IJEUOTHELLO_H
#define IJEUOTHELLO_H

#include "jeu.h"
#include "grilleothello.h"

class IJeuOthello: public Jeu
{
public:
    IJeuOthello(Joueur& j1,Joueur& j2);
    virtual void deroulementPartie()=0;
    virtual bool choixCase(const int joueur)=0;
    GrilleOthello grille=GrilleOthello();
};

#endif // IJEUOTHELLO_H


