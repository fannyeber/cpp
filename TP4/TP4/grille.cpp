#include "grille.h"

/**
 * @brief Grille::Grille
 * @param nb_ligne : nombre de lignes que comporte la grille
 * @param nb_colonne : nombre de colonne que la grille comporte
 * @param nb_alignement_victoire : nombre de jetons a aligner pour pouvoir gagner
 */
Grille::Grille(const int nb_ligne,const int nb_colonne,const int nb_alignement_victoire):NB_LIGNES(nb_ligne),NB_COLONNES(nb_colonne), NB_ALIGNEMENT_VICTOIRE(nb_alignement_victoire)
{
    this->init();
}

/**
 * @brief Grille::Grille
 * @param nb_ligne : nombre de lignes que comporte la grille
 * @param nb_colonne : nombre de colonne que la grille comporte
 */
Grille::Grille(const int nb_ligne,const int nb_colonne): NB_LIGNES(nb_ligne),NB_COLONNES(nb_colonne), NB_ALIGNEMENT_VICTOIRE(-1)
{
    this->init();
}

/**
 * @brief Grille::init initialise la grille avec des 0 dans toutes les cases
 */
void Grille::init(){
    grille.assign(NB_LIGNES,std::vector<int>(NB_COLONNES,0));
}

/**
 * @brief Grille::isVide
 * @param i : ligne
 * @param j : colonne
 * @return vrai si la case i,j est égale à 0, faux sinon
 */
bool Grille::isVide(const int i,const int j) const
{
    if(i>=0 && j>=0 && i<NB_LIGNES && j<NB_COLONNES){
        return this->grille[i][j]==0;
    }
    return false;
}

bool Grille::isJoueur(const int joueur,const int i, const int j) const{
    if(i>=0 && j>=0 && i<NB_LIGNES && j<NB_COLONNES){
        return this->grille[i][j]==joueur;
    }
    return false;
}

/**
 * @brief Grille::isRempli
 * @return vrai si aucune case du tableau n'est égale à 0, faux sinon
 */
bool Grille::isRempli()const{
    for(int i=0;i<NB_LIGNES;i++){
        for(int j=0;j<NB_COLONNES;j++){
            if(this->isVide(i,j)){
                return false;
            }
        }
    }
    return true;
}

/**
 * @brief Grille::ligneComplete
 * @param i : ligne
 * @param joueur : indice du joueur
 * @return vrai si le joueur à une alignement sur une ligne, faux sinon
 */
bool Grille::ligneComplete(const int i,const int joueur) const
{
    int compteur=0;
    bool reussite=false;
    for(int j=0;j<NB_COLONNES && !reussite;j++){
        if(this->grille[i][j]!=joueur){
            compteur=0;
        }
        else{
        compteur++;
        }
        if(compteur==NB_ALIGNEMENT_VICTOIRE){
            reussite=true;
        }
    }
    return reussite;
}

/**
 * @brief Grille::colonneComplete
 * @param j : colonne
 * @param joueur : indice du joueur
 * @return vrai si le joueur a un alignement sur une colonne, faux sinon
 */
bool Grille::colonneComplete(const int j,const int joueur) const
{
    int compteur=0;
    bool reussite=false;
    for(int i=0;i<NB_LIGNES && !reussite;i++){
        if(this->grille[i][j]!=joueur){
            compteur=0;
        }
        else{
            compteur++;
        }
        if(compteur==NB_ALIGNEMENT_VICTOIRE){
            reussite=true;
        }
    }
    return reussite;
}

/**
 * @brief Grille::diagonalComplete
 * @param joueur : indice du joueur
 * @return vrai si le joueur a une alignement sur une diagonale, faux sinon
 */
bool Grille::diagonalComplete(const int joueur) const
{
    int limite=NB_COLONNES-NB_ALIGNEMENT_VICTOIRE;
    int compteur=0;
    for (int i=limite; i<NB_LIGNES; i++){
        for (int j=0; j<NB_COLONNES-limite; j++){
            compteur=0;
            for(int k=0;k<NB_ALIGNEMENT_VICTOIRE && i-k>=0 && j+k<NB_COLONNES;k++){
                if(this->grille[i-k][j+k]!=joueur){
                    compteur=0;
                }
                else{
                    compteur++;
                }

                if(compteur==NB_ALIGNEMENT_VICTOIRE){
                    return true;
                }
            }
        }
    }

    for (int i=limite; i<NB_LIGNES; i++){
        for (int j=limite; j<NB_COLONNES; j++){
            compteur=0;
            for(int k=0;k<NB_ALIGNEMENT_VICTOIRE && i-k>=0 && j-k>=0;k++){
                if(this->grille[i-k][j-k]!=joueur){
                    compteur=0;
                }
                else{
                    compteur++;
                }
                if(compteur==NB_ALIGNEMENT_VICTOIRE){
                    return true;
                }
            }
        }
    }
    return false;

}

/**
 * @brief Grille::victoireJoueur
 * @param joueur : indice du joueur
 * @return vrai si le joueur a gagné, faux sinon
 */
bool Grille::victoireJoueur(const int joueur) const
{

    for(int i=0;i<NB_LIGNES;i++){

        if(ligneComplete(i,joueur)){
            return true;
        }
    }

    for(int i=0;i<NB_COLONNES;i++){

        if(colonneComplete(i,joueur)){
            return true;
        }
    }
    return (diagonalComplete(joueur) || diagonalComplete(joueur));
}

/**
 * @brief Grille::afficheGrille affiche la grille
 * _ pour les cases vides
 * X pour le premier joueur
 * O pour le deuxième joueur
 */
void Grille::afficheGrille() const
{
    std::cout<<std::endl;
    for(int i=0;i<NB_LIGNES;i++){
        for(int j=0;j<NB_COLONNES;j++){
            if(this->grille[i][j]==0){
                std::cout<<"_ ";
            }
            else if(this->grille[i][j]==1){
                std::cout<<"X ";
            }
            else{
                std::cout<<"O ";
            }
        }
        std::cout<<std::endl;
    }
    std::cout<<std::endl;
}



