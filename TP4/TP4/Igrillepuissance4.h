#include "grille.h"

class Igrillepuissance4 : public Grille
{
public:

	Igrillepuissance4();
	virtual bool deposerJeton(const int joueur, const int j) = 0;
};

