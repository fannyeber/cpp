#ifndef JEUOTHELLO_H
#define JEUOTHELLO_H

#include "ijeuothello.h"

class JeuOthello: public IJeuOthello
{
public:
    JeuOthello(Joueur& j1,Joueur& j2);
    void deroulementPartie();
    bool choixCase(const int joueur);
};

#endif // JEUOTHELLO_H
