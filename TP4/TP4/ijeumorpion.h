#ifndef IJEUMORPION_H
#define IJEUMORPION_H

#include "jeu.h"
#include "grillemorpion.h"

class IJeuMorpion: public Jeu
{
public:
    IJeuMorpion(Joueur& j1,Joueur& j2);
    virtual void deroulementPartie()=0;
    virtual bool choixCase(const int joueur)=0;
    GrilleMorpion grille=GrilleMorpion();
};

#endif // IJEUMORPION_H
