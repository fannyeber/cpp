#include "jeu.h"

/**
 * @brief Jeu::Jeu : lance le déroulement de la partie
 * @param _grille : grille du jeu auxquel les joueurs ont décidé de jouer
 */
Jeu::Jeu(Grille& _grille, Joueur& joueur1, Joueur& joueur2):j1(joueur1),j2(joueur2),grille(_grille)
{

}


/**
 * @brief Jeu::stringIsInt
 * @param str : chaine de caractères
 * @return vrai si la chaine de caractère est un nombre, faux sinon
 */
bool Jeu::stringIsInt(const std::string str) const{
    try {
        std::stoi(str);
        return true;
    } catch (...) {
        return false;
    }
}

/**
 * @brief Jeu::verificationSaisie
 * @param i : nombre
 * @return vrai si le nombre ne dépasse pas le tablea, faux sinon
 */
bool Jeu::verificationSaisieColonne(const int j) const
{
    if(j<0 || j>this->grille.getNB_COLONNES()-1){
        return false;
    }
    return true;
}

/**
 * @brief Jeu::verificationSaisie
 * @param i : nombre
 * @return vrai si le nombre ne dépasse pas le tablea, faux sinon
 */
bool Jeu::verificationSaisieLigne(const int i) const
{
    if(i<0 || i>this->grille.getNB_LIGNES()-1){
        return false;
    }
    return true;
}

/**
 * @brief Jeu::finPartie
 * @return vrai si un joueur a gagné la partie ou si il y a match nul, faux sinon
 */
bool Jeu::finPartie()
{
    //si quelqu'un a gagné
    if(grille.victoireJoueur(1)){
        felicitation(1);
        return true;
    }
    else if(grille.victoireJoueur(2)){
        felicitation(2);
        return true;
    }
    //si la grille est pleine
    else if (grille.isRempli()){
        matchNul();
        return true;
    }
    else{
        return false;
    }
}

/**
 * @brief Jeu::felicitation
 * @param gagnant : indice du gagnant
 */
void Jeu::felicitation(const int gagnant) const
{
    if(gagnant==1){
        std::cout<<std::endl<<"Nous avons un VAINQUEUR ! Bravo "<<j1.getNom()<<" !!!"<<std::endl;
    }
    else{
        std::cout<<std::endl<<"Nous avons un VAINQUEUR ! Bravo "<<j2.getNom()<<" !!!"<<std::endl;
    }
}

/**
 * @brief Jeu::matchNul en cas de match nul on demande au joueurs si ils veulent rejouer
 */
void Jeu::matchNul()
{
    std::cout<<std::endl<<"La grille est rempli...match nul, vous voulez rejouer ? Y/N"<<std::endl;
    std::string saisie;
    std::cin>>saisie;

    if(saisie=="Y"){
        std::cout<<"Allez on joue alors ! "<<std::endl;
        this->grille.init();
        deroulementPartie();
    }
    else{
        std::cout<<"Merci d'avoir joué, à la prochaine"<<std::endl;
    }

}

