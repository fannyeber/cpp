#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "Igrillepuissance4.h"


class GrillePuissance4:public Igrillepuissance4
{
public:
    GrillePuissance4();
    bool deposerJeton(const int joueur,const int j);
};

#endif // GRILLEPUISSANCE4_H
