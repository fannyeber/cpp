#ifndef GRILLE_H
#define GRILLE_H

#include <stdbool.h>
#include <vector>
#include <iostream>

class Grille
{
public:
    Grille(const int nb_ligne,const int nb_colonne,const int nb_alignement_victoire);
    Grille(const int nb_ligne,const int nb_colonne);
    void init();
    bool isVide(const int i,const int j) const;
    bool isJoueur(const int joueur,const int i, const int j) const;
    bool isRempli() const;
    virtual bool victoireJoueur(const int joueur) const;
    void afficheGrille() const;

    const std::vector<std::vector<int>> &getGrille() const{
        return grille;
    }
    void setGrille(const std::vector<std::vector<int>> &newGrille){
        grille=newGrille;
    }

    int getNB_COLONNES() const
    {
        return NB_COLONNES;
    }

    int getNB_LIGNES() const
    {
        return NB_LIGNES;
    }

protected:
    std::vector<std::vector<int>> grille;
    const int NB_LIGNES;
    const int NB_COLONNES;

private:
    const int NB_ALIGNEMENT_VICTOIRE;
    bool ligneComplete(const int i,const int joueur) const;
    bool colonneComplete(const int j,const int joueur) const;
    bool diagonalComplete(const int joueur) const;

};

#endif // GRILLE_H
