#ifndef IJEUPUISSANCE4_H
#define IJEUPUISSANCE4_H

#include "jeu.h"
# include "grillepuissance4.h"

class IJeuPuissance4:public Jeu
{
public:
    IJeuPuissance4(Joueur& j1,Joueur& j2);
    virtual void deroulementPartie()=0;
    virtual bool choixCase(const int joueur)=0;
    GrillePuissance4 grille=GrillePuissance4();
};

#endif // IJEUPUISSANCE4_H
