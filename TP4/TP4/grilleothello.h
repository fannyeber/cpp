#ifndef GRILLEOTHELLO_H
#define GRILLEMOTHELLO_H

#include "Igrilleothello.h"

class GrilleOthello : public IGrilleOthello
{
public:
    GrilleOthello();
    void initOthello();
    bool deposerJeton(const int joueur,const int j,const int i);
    bool possibiliteDeJouer(const int joueur);
    bool encadrementHorizontal(const int joueur,const int i, const int j, const bool flip);
    bool encadrementVertical(const int joueur,const int i, const int j, const bool flip);
    bool encadrementDiagonal(const int joueur,const int i, const int j, const bool flip);
    bool victoireJoueur(const int joueur) const;

};

#endif //GRILLEOTHELLO_H

