QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        grille.cpp \
        grillemorpion.cpp \
        grillepuissance4.cpp \
        grilleothello.cpp \
        ijeumorpion.cpp \
        ijeuothello.cpp \
        ijeupuissance4.cpp \
        Igrillemorpion.cpp \
        Igrillepuissance4.cpp \
        Igrilleothello.cpp \
        jeu.cpp \
        jeumorpion.cpp \
        jeuothello.cpp \
        jeupuissance4.cpp \
        joueur.cpp \
        menu.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    grille.h \
    grillemorpion.h \
    grillepuissance4.h \
    grilleothello.h \
    ijeumorpion.h \
    ijeuothello.h \
    ijeupuissance4.h \
    Igrillemorpion.h \
    Igrillepuissance4.h \
    Igrilleothello.h \
    jeu.h \
    jeumorpion.h \
    jeuothello.h \
    jeupuissance4.h \
    joueur.h \
    menu.h \
    joueur.h
