#ifndef JEUPUISSANCE4_H
#define JEUPUISSANCE4_H

#include "ijeupuissance4.h"

class JeuPuissance4:public IJeuPuissance4
{
public:
    JeuPuissance4(Joueur& j1,Joueur& j2);
    void deroulementPartie();
    bool choixCase(const int joueur);

};

#endif // JEUPUISSANCE4_H
