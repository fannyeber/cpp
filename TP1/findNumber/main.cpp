#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <stdbool.h>

int main()
{
    int number=(std::srand()%1000)+1;
    std::string userNumber;
    int count=0;

    std::cout<<"Deviner un nombre entre 0 et 1000 : ("<<number<<")"<<std::endl;

    do{
        std::cin>>userNumber;
        count++;
        if(stoi(userNumber)<number){
            std::cout<<"C'est PLUS"<<std::endl;
        }
        else if(stoi(userNumber)>number){
            std::cout<<"C'est MOINS"<<std::endl;
        }
        else{
            std::cout<<"VOUS AVEZ GAGNE (en "<<count<<" tentatives)"<<std::endl;
        }

    }
    while(number!=std::stoi(userNumber));


}
