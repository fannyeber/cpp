#include <stdlib.h>
#include <iostream>
#include <array>
#include <cstdlib>

extern int n;

void displayTab(std::array<int,10> tab);
int sum(int a, int b);
void inverse(int &a,int &b);
void replacePointeurs(int a, int b, int *c);
void replaceReference(int a, int b, int &c);
std::array<int,10> createTab();
void tri(std::array<int,10> &tab);
void askUser(int &n,bool &triCroi);
