#include "numbers.h"

int main()
{
    int a=5;
    int b=6;
    int c= 8;

    std::cout << "I.1.1 : 5 + 6 =" << sum(a,b) <<std::endl;

    inverse(a,b);
    std::cout << "I.1.2 : a=5 et b=6 ... a="<<a<<" et b="<<b << std::endl;

    replacePointeurs(a,b,&c);
    std::cout << "I.1.3 : pointeurs c= 5+6 ="<<c<<std::endl;
    c=0;

    replaceReference(a,b,c);
    std::cout << "I.1.3 : references c= 5+6 ="<<c<<std::endl;

    // bool triCroi=true;
    // askUser(SIZE,triCroi);
    std::array<int,10> tab=createTab();
    std::cout << "I.1.4 : tableau = ";
    displayTab(tab);

    tri(tab);
    std::cout << "tableau apres tri =";
    displayTab(tab);

}
