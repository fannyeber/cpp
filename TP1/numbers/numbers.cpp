#include "numbers.h"



void displayTab(std::array<int, 10> tab){
    for(unsigned int i=0;i<tab.size();i++){
        std::cout << tab[i] << ";";
    }
    std::cout <<std::endl;
}
// I.1.1 : fonction qui renvoie la somme de deux entiers
int sum(int a, int b){
    return a+b;
}

//I.1.2 : méthode qui prend en paramètre deux entiers et qui inverse leur valeur
void inverse(int &a, int &b){
    int c = a;
    a=b;
    b=c;
}

// I.1.3 : Ecrivez une autre fonction qui prenne en paramètre trois entiers, et qui remplace la
// valeur du troisième par la somme des deux premiers. Faites ceci deux fois, une fois à
// l’aide des pointeurs, une fois à l’aide des références.
void replacePointeurs(int a, int b, int *c){
    int d=b+a;
    *c=d;
}

void replaceReference(int a, int b, int &c){
    c=a+b;
}

// I.1.4 Ecrivez un programme générant un tableau d’entiers rempli de valeurs aléatoires toutes
// positives.
std::array<int, 10> createTab(){
    std::array<int,10> tab;

    for(unsigned int i=0;i<tab.size();i++){

        tab[i]=rand()%100;
    }
    return tab;
}

// Affichez les valeurs successives du tableau dans la console. En utilisant une des
// méthodes écrites pour la question 1.2, effectuez un tri du tableau par ordre croissant et
// affichez le dans la console.
void tri(std::array<int, 10> &tab){
    //pas super ouf niveau perf sur des très grands tableaux (à chaque changement on recommence du début)
    for(unsigned int i=1;i<tab.size();i++){
        if(tab[i-1]>tab[i]){
            inverse(tab[i-1],tab[i]);
            i=0;
        }
    }
}

// Bonus : Faites en sortes que la taille du tableau soit générée en fonction d’une saisie
// de l’utilisateur, et que le tri soit croissant ou décroissant en fonction d’une saisie
// utilisateur également.
// void askUser(int &n,bool &triCroi){
//     std::cout << "Taille du tableau ? : " ;
//     std::string taille;
//     std::cin >>  taille;
//     std::cout << "Voulez-vous que le tri soit croissant : Y/N";
//     std::string tri;
//     std::cin >> tri;

//     n=static_cast<int>(taille);
//     if(tri==N){
//         triCroi=false;
//     }

// }
