#include <stdlib.h>
#include <iostream>
#include <string>
#include <algorithm>

int main()
{
    // III.1.1
    std::string lastName;
    std::string firstName;

    std::cout << "Entrez votre prenom :";
    std::cin>>firstName;
    std::cout<<"Bonjour "<< firstName <<", passe une bonne journée!"<<std::endl;

    // III.1.2
    std::cout<< "Entrez votre nom puis votre prénom : ";
    std::cin>>lastName>>firstName;
    std::cout<< "Bonjour "<<firstName<<" "<<lastName<<std::endl;

    // III. Bonus
    //std::cout<< "Bonjour"<< std::transform(firstName.begin(),::toupper);
}
