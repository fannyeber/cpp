#ifndef JEU_H
#define JEU_H

#include "joueur.h"
#include "grillemorpion.h"
#include "grillepuissance4.h"
#include <iostream>
#include <stdlib.h>
#include <string>

class Jeu
{
public:
    Jeu(Grille* _grille);

private:
    Joueur j1=Joueur(1);
    Joueur j2=Joueur(2);
    Grille* grille;
    void deroulementPartie();
    void debutPartie();
    bool choixCase(const int joueur);
    bool stringIsInt(const std::string str) const;
    bool verificationSaisie(const int i) const;
    bool finPartie();
    void felicitation(const int gagnant) const;
    void matchNul();
};

#endif // JEU_H
