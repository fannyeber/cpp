#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include "grille.h"


class GrilleMorpion:public Grille
{
public:
    GrilleMorpion();
    bool deposerJeton(const int joueur,const int j,const int i=-1);

};

#endif // GRILLEMORPION_H
