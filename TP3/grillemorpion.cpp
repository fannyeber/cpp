#include "grillemorpion.h"

/**
 * @brief GrilleMorpion::GrilleMorpion
 */
GrilleMorpion::GrilleMorpion(): Grille(3,3,3)
{
}

/**
 * @brief GrilleMorpion::deposerJeton
 * @param joueur : indice du joueur qui veut deposer le jeton
 * @param j : colonne
 * @param i : ligne
 * @return vrai si il a pu déposer le jeton, faux sinon
 */
bool GrilleMorpion::deposerJeton(const int joueur, const int j, const int i)
{
    if(this->isVide(i,j)){
        this->grille[i][j]=joueur;
        return true;
    }
    return false;
}

