#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "grille.h"


class GrillePuissance4:public Grille
{
public:
    GrillePuissance4();
    bool deposerJeton(const int joueur,const int j,const int i=-1);
};

#endif // GRILLEPUISSANCE4_H
