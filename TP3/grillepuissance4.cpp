#include "grillepuissance4.h"

/**
 * @brief GrillePuissance4::GrillePuissance4
 */
GrillePuissance4::GrillePuissance4(): Grille(4,7,4)
{
}

/**
 * @brief GrillePuissance4::deposerJeton
 * @param joueur : indice du joueur qui veut déposer le jeton
 * @param j : colonne
 * @param i : ligne
 * @return vrai si il a pu déposer le jeton, faux sinon
 */
bool GrillePuissance4::deposerJeton(const int joueur, const int j, const int i)
{
    for(int i=NB_LIGNES-1;i>=0;i--){
        if(this->isVide(i,j)){
            this->grille[i][j]=joueur;
            return true;
        }
    }

    return false;
}
