#ifndef JOUEUR_H
#define JOUEUR_H

#include <string>

class Joueur
{
public:
    Joueur(const int id);

    int getIdentifiant() const
    {
        return identifiant;
    }

    const std::string getNom() const
    {
        return nom;
    }

    void setNom(const std::string &newNom)
    {
        nom = newNom;
    }


private:
    const int identifiant;
    std::string nom;
};

#endif // JOUEUR_H
