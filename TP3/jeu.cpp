#include "jeu.h"

/**
 * @brief Jeu::Jeu : lance le déroulement de la partie
 * @param _grille : grille du jeu auxquel les joueurs ont décidé de jouer
 */
Jeu::Jeu(Grille* _grille):grille(_grille)
{
    deroulementPartie();
}

/**
 * @brief Jeu::deroulementPartie : ensemble des étapes dans un jeu
 */
void Jeu::deroulementPartie()
{
    debutPartie();
    grille->afficheGrille();

    //Commencer la partie avec un tour par tour et une verification de la victoire à chaque tour
    bool fin=false;
    bool deposeJeton=false;
    while(!fin){ //Tant que aucun joueur a gagné ou qu'il n'y a pas de match nul on continue à jouer
        for(int i=1;i<3 && !fin ;i++){ //car il y a deux joueurs
            while(!deposeJeton){ //Tant que le joueur n'a pas réussi a poser un jeton il pourra rééssayer a poser
                deposeJeton=choixCase(i);
                if(!deposeJeton){
                    std::cout<<"ERREUR : Il faut rentrer un chiffre entre 0 et "<<this->grille->getNB_COLONNES()<<" sur une case vide"<<std::endl;
                }
            }

            grille->afficheGrille();
            fin=this->finPartie(); //on verifie si quelqu'un a gagné ou si il y a match nul
            deposeJeton=false;
        }
    }

}

/**
 * @brief Jeu::debutPartie
 * Message de bienvenue et accueil des joueurs
 */
void Jeu::debutPartie() {
    std::cout<<"Bonjour ! Bienvenue dans le jeu de morpion le plus opti du monde ! (c'est faux)"<<std::endl<<std::endl;

    for(int i=1;i<3;i++){
        std::cout<<"Joueur "<<i<<" : Entre ton prenom "<<std::endl;
        std::string prenom;
        std::cin>>prenom;
        if(i==1)
            j1.setNom(prenom);
        else
            j2.setNom(prenom);
    }

    std::cout<<std::endl<<"C'EST PARTI ! "<<std::endl;

}

/**
 * @brief Jeu::choixCase : demande au joueur de placer son pion sur une case
 * en saisissant ligne et colonne ou seulement colonne en fonction de la grille
 * @param joueur : indice du joueur
 * @return vrai si le joueur a réussi a placer son jeton, faux sinon
 */
bool Jeu::choixCase(const int joueur)
{
    std::string nom;
    if(joueur==1){
        nom=j1.getNom();
    }
    else{
        nom=j2.getNom();
    }
    std::string colonne;
    if (dynamic_cast<GrilleMorpion*>(this->grille) != 0){
        std::string ligne;
        std::cout<<"Joueur "<<nom<<", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::cin>>ligne>>colonne;
        if(!stringIsInt(ligne) || !stringIsInt(colonne)){
            return false;
        }
        int l=std::stoi(ligne);
        int c=std::stoi(colonne);

        if(verificationSaisie(l)&&verificationSaisie(c)){
            return grille->deposerJeton(joueur,c,l);
        }
        return false;
    }
    else if (dynamic_cast<GrillePuissance4*>(this->grille) != 0){

        std::cout<<"Joueur "<<joueur<<", entre le numero de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::cin>>colonne;
        if(!stringIsInt(colonne)){
            return false;
        }
        int c=std::stoi(colonne);

        if(verificationSaisie(c)){
            return grille->deposerJeton(joueur,c);
        }
        return false;
    }
    throw std::invalid_argument( "ce n'est ni une grille de puissance4 ni une grille de morpion.....Oh no je suis perdu!" );

}

/**
 * @brief Jeu::stringIsInt
 * @param str : chaine de caractères
 * @return vrai si la chaine de caractère est un nombre, faux sinon
 */
bool Jeu::stringIsInt(const std::string str) const{
    try {
        std::stoi(str);
        return true;
    } catch (...) {
        return false;
    }
}

/**
 * @brief Jeu::verificationSaisie
 * @param i : nombre
 * @return vrai si le nombre ne dépasse pas le tablea, faux sinon
 */
bool Jeu::verificationSaisie(const int i) const
{
    if(i<0 || i>this->grille->getNB_COLONNES()){
        return false;
    }
    return true;
}

/**
 * @brief Jeu::finPartie
 * @return vrai si un joueur a gagné la partie ou si il y a match nul, faux sinon
 */
bool Jeu::finPartie()
{
    //si quelqu'un a gagné
    if(grille->victoireJoueur(1)){
        felicitation(1);
        return true;
    }
    else if(grille->victoireJoueur(2)){
        felicitation(2);
        return true;
    }
    //si la grille est pleine
    else if (grille->isRempli()){
        matchNul();
        return true;
    }
    else{
        return false;
    }
}

/**
 * @brief Jeu::felicitation
 * @param gagnant : indice du gagnant
 */
void Jeu::felicitation(const int gagnant) const
{
    if(gagnant==1){
        std::cout<<std::endl<<"Nous avons un VAINQUEUR ! Bravo "<<j1.getNom()<<" !!!"<<std::endl;
    }
    else{
        std::cout<<std::endl<<"Nous avons un VAINQUEUR ! Bravo "<<j2.getNom()<<" !!!"<<std::endl;
    }
}

/**
 * @brief Jeu::matchNul en cas de match nul on demande au joueurs si ils veulent rejouer
 */
void Jeu::matchNul()
{
    std::cout<<std::endl<<"La grille est rempli...match nul, vous voulez rejouer ? Y/N"<<std::endl;
    std::string saisie;
    std::cin>>saisie;

    if(saisie=="Y"){
        std::cout<<"Allez on joue alors ! "<<std::endl;
        deroulementPartie();
    }
    else{
        std::cout<<"Merci d'avoir joué, à la prochaine"<<std::endl;
    }

}

